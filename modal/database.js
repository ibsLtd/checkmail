const mysql = require('mysql');
require('dotenv').config()

const db_config = {
    host: process.env.DB_HOST,
    user:  process.env.DB_USER,
    password:process.env.DB_PASS,
    database:  process.env.DB_NAME,
    connectTimeout: 10000

};
const pool = mysql.createPool(db_config);

pool.on('error', (err) => {
    console.log(err.code);
});

module.exports = pool;
