import React from 'react'

const WhyChooseUs = () =>{


    return(
        <div className={' why-choose-us  padding-btm'}>
            <div className={'container'}>
                <div className={'row'}>
                    <div className={'col-12 padding-btm text-center'}>
                        <h1>{'Sending emails is not enough.'}</h1>
                        <h3>{'You have to deliver.'}</h3>
                        <p>{'An email address is your first line of communication with a customer. Make sure you reach them.'}</p>
                    </div>
                    <div className={'col-md-4  text-center'}>
                        <div className={'bg-img email'}/>

                        <h4>{'Improve email data quality'}</h4>
                        <p>{'You work hard to acquire email addresses and to protect your email reputation. Fake or stale email addresses negatively impact this reputation, jeopardizing your connection with valid email subscribers. Ensure your email addresses are valid before hitting send and maintain your solid email reputation.'}</p>
                    </div>
                    <div className={'col-md-4  text-center'}>
                        <div className={'bg-img customer'}/>
                        <h4>{'Maximize the customer experience'}</h4>
                        <p>{'When it comes to our solutions, OK isn’t good enough. When your consumers provide their contact information, they expect to hear from you. When they don’t, you are missing out on a potential relationship and conversion.'}</p>
                    </div>
                    <div className={'col-md-4  text-center'}>
                        <div className={'bg-img roi'}/>
                        <h4>{'Increase ROI'}</h4>
                        <p>{'A fake email is not just a lost opportunity. 25% of all email addresses collected are invalid, making it a financial liability. Sending to valid addresses increases engagement rates, improves reputation with mailbox providers, and drives more conversions'}</p>
                    </div>

                </div>
            </div>
        </div>
    )
}

export  default  WhyChooseUs