import React from 'react'
import Link from 'next/link'

const Header = props => {


    return (
        <header>
            <div className={'container'}>
                <div className={'row justify-content-between  align-items-center'}>
                    <Link href={{pathname: '/'}} as={`/`}>
                        <a><img className={'site-logo'} src={`/assets/logo.png`} alt={'iBS | Informatics Business Services'}/></a>
                    </Link>
                </div>
            </div>
        </header>
    )
}

export default Header
