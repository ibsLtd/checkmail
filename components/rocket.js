import React,{useEffect,useState} from 'react'


const Rocket = props =>{

    const [ scrolled, setScrolled ] = useState(0)

    useEffect(() => {
        const element = document.querySelector('.rocket-svg')
        const handleScroll = () => {
            const footer =  document.getElementById("footer");
            console.log(footer.offsetTop)
            console.log(window.pageYOffset)
            // if(footer.offsetTop <= window.pageYOffset){
            //     setScrolled(window.pageYOffset)
            //
            // }
        }
        window.addEventListener('scroll', handleScroll)
        return () => {
            window.removeEventListener('scroll', handleScroll)
        }
    }, [])

    return(
        <div className={'pos-abs rocket-div lines'}>
            <div className={'bg-img rocket-svg'} style={{transform:`translateY(${scrolled}px)`}}/>
        </div>
    )
}

export default Rocket