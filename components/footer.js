import React from 'react'


const Footer = props => {


    return (
        <footer id ={'footer'}>
            <div className={'container'}>
                <div className={'row align-items-center'}>
                    <div className={'col-md-6'}>
                        <p>{'Copyright (c) 2020 Novatech Solutions, All rights reserved'}</p>

                    </div>
                    <div className={'col-md-6 d-flex justify-content-end'}>
                        <div className={'social-icons'}>
                            <a href={'https://www.linkedin.com/company/ibs-piraeus'} rel={"noopener"} target={"_blank"}><i className="linkedin"/></a>
                            <a href={'https://www.facebook.com/ibs.gr/?ref=br_rs'} rel={"noopener"} target={"_blank"}><i className="facebook"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer