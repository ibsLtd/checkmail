import React from 'react'

const Overview = () => {

    return (
        <>
            <div className={'col-12 clr-white padding-btm text-center'}>
                <h1>{'Full-Featured Email Verification'}</h1>
                <p>{'Get rid of spam traps, hard bounces, disposable or catch-all emails without breaking your budget.'}</p>
            </div>

            <div className={'col-md-6 clr-white'}>
                <ul>
                    <li >
                        <h2 className={'pos-rel'}>{'Create a FREE account'}</h2>
                    </li>
                    <li>
                        <h2 className={'pos-rel'}>{'Upload your dirty list'}</h2>
                        <p>{'We accept CSV and TXT  '}</p>
                    </li>
                    <li>
                        <h2 className={'pos-rel'}>{'Download a clean list'}</h2>
                        <p>{'You will be notified within a few minutes.'}</p>
                    </li>
                </ul>


            </div>
        </>
    )
}

export default Overview