import React, {useState, useEffect} from 'react'
import axios from 'axios';


const Form = (props) => {

    const initialState = {name: '', email: '', textarea: '', captcha: ''}
    const [state, setState] = useState({...initialState}),
        [errorState, setErrors] = useState({name: '', email: '',success:'',fail:''});


    const handleInput = e => {
        state[e.target.id] = e.target.value
        setState({...state})
    }

    const handleValidation = (fields) => {
        let formIsValid = true;
        let errors = {};

        //Name
        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "Cannot be empty";
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        } else {
            let regex = /\S+@\S+\.\S+/
            formIsValid = regex.test(fields["email"]);
            if (!formIsValid) errors["email"] = "Email is not valid";
        }
        errorState.name = errors.name
        errorState.email = errors.email
        setErrors({...errorState})
        return formIsValid;
    }

    const reset = () => {
        setState({...initialState})
    }


    const onSubmit = e => {
        e.preventDefault()
        const valid = handleValidation(state)
        if (valid) {
            const data = {
                name: state.name,
                message: state.textarea,
                _replyto: state.email,
                _subject: state.subject
            }
            axios.post('https://formspree.io/xqkbqgoz', data, {
                crossDomain: true,
                dataType: "json",
            })
                .then(res => {

                    if (res.data.ok) {
                        errorState.success = 'Thank you for getting in touch!'
                    } else {
                        errorState.fail = 'something went wrong, try again later'
                    }
                    reset()

                })
                .catch(err => {
                    console.log(err)
                })

        }

    }


    return (
        <form>

            <div className={" d-flex flex-column form-group"}>
                <input type={"text"} id={"name"} onChange={handleInput} placeholder={'FullName'}/>
            </div>
            <div className="d-flex flex-column form-group">
                <input type={"email"} id={"email"} onChange={handleInput} placeholder={'Email'}/>
            </div>
            <div className={" d-flex flex-column form-group"}>
                <input type={"text"} id={"subject"} onChange={handleInput} placeholder={'Subject'}/>
            </div>
            <div className="d-flex flex-column  form-group">
                <textarea id={"textarea"} onChange={handleInput} placeholder={'Message'}/>
            </div>

            <div className={'pr-button'} onClick={onSubmit}>{'Submit'}</div>
            {(errorState.name !== '' || errorState.email !== '') &&
            <div className={'error'}><p className={'clr-white'}>{errorState.name || errorState.email}</p></div>
            }
            {errorState.fail  !== '' || errorState.success  !== '' &&
            <div className={'error'}><p className={'clr-white'}>{errorState.fail || errorState.success}</p></div>
            }
        </form>


    )
}

export default Form