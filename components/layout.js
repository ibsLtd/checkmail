import React,{useEffect} from "react";
import NextHead from 'next/head'
import Header from './header'
import Footer from "./footer";


const Layout = props => {

    useEffect(()=>{

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/service-worker.js')
                .then(registration => {
                    console.log('Service Worker registration successful')
                })
                .catch(err => {
                    console.warn(err.message)
                })
        }
    })
    return (
        <React.Fragment>

            <NextHead>
                <meta charSet="UTF-8"/>
                <title>{props.title || ''}</title>
                <meta name="description" content={props.description || ''}/>
                <meta name="keywords" content={props.keywords || ''}/>
                <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
                <meta name=" theme-color" content="#fff"/>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"/>
                <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'/>

                <link type="text/css" rel="stylesheet" href={`/public/modules.css`}/>
                <link type="text/css" rel="stylesheet" href={`/public/main.css`}/>
            </NextHead>

            <Header/>
                {props.children}

            <Footer/>

        </React.Fragment>
    )

}

export default Layout