import React, {useContext, useState} from 'react'
import axios from 'axios'
import Router from 'next/router';


const Form = props => {
    const [email, setEmail] = useState('')
    const [error, setError] = useState('')
    const [isValid, setValid] = useState(false)


    const handleValidation = (email) => {
        let formIsValid = true;
        let error = '';

        if (email === '') {
            formIsValid = false;
            error = "Cannot be empty";
        } else {
            let regex = /\S+@\S+\.\S+/
            formIsValid = regex.test(email);
            if (!formIsValid) error = "Email is not valid";

        }

        return {isValid: formIsValid, error: error}
    }
    const handleInput = e => {
        e.preventDefault()
        const process = handleValidation(e.target.value)
        if (process.isValid) setEmail(e.target.value)
        setError(process.error)
        setValid(process.isValid)
    }

    const handleSubmit = e => {
        e.preventDefault()
        const ip = props.ip
        if (isValid) {
            axios.post('/', {email, ip})
                .then(function (res) {
                    if (res.data === null) setError('user exist please try with another email')
                    else Router.push('/upload', '/upload', {shallow: true});
                })
                .catch(function (err) {
                    console.log(err)
                });
        } else {
            setError('You must enter your email')

        }

    }
    return (
        <form className={'d-flex flex-column register'}>
            <input type="email" onChange={handleInput} placeholder={'Enter Email'}/>
            <div className={'pr-button'} onClick={handleSubmit}>{"Create Free account"}</div>
            <div className={'error clr-white'}><p>{error}</p></div>
        </form>
    )


}

export default Form