import React from 'react'


const Banner = props=>{

    return (
        <div className={'pos-rel  banner-section'}>
            <div className={`bg-img banner ${props.height}`}/>
            <div className={`pos-abs banner-shape ${props.class}`}>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none"
                     className={'pos-abs centerX'}>
                    <path className="shape-fill" d="M0,6V0h1000v100L0,6z"/>
                </svg>
            </div>
        </div>
    )
}

export default Banner