import React, {useState} from 'react'
import axios from 'axios'
import Router from "next/router";


const UploadForm = props => {

    let cnt = 0;
    let timeOut = null;
    const [file, setFile] = useState(''),
        [maxValue, setMaxValue] = useState(30),
        [streamValue, setStreamValue] = useState(30),
        [dns, setDns] = useState(true),
        [comma, setComma] = useState(false),
        [error, setError] = useState(''),
        [req, setReq] = useState({}),
        [loader, setLoader] = useState({}),
        [isValid, setValid] = useState(false);

    const options = {
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    }

    const handleFile = e => {
        const pattern = new RegExp("^.+\.(xlsx|xls|csv|txt)$")
        if (pattern.test(e.target.files[0].name)) {
            setFile(e.target.files[0])
            setValid(true)
        } else {
            setError('We accept files only csv or txt')
        }
    }
    const handleRange = e => {
        if (e.target.name === 'max_conn_timeout') {
            setMaxValue(e.target.value)
        } else {
            setStreamValue(e.target.value)
        }
    }
    const handleCheckBox = e => {
        if (e.target.name === 'dns_lookup_only') {
            setDns(!dns)
        } else {
            setComma(!comma)
        }
    }
    const handleSubmit = e => {
        e.preventDefault();
        let cookie = getCookie('user')
        if (isValid) {
            const data = new FormData();
            data.append('initial_file', file, file.name)
            data.append('email_from', decodeURIComponent(cookie))
            data.append('max_conn_timeout', maxValue)
            data.append('stream_timeout', streamValue)
            data.append('dns_lookup_only', dns)
            data.append('comma_delimiter', comma)
            axios.post(`${process.env.DOMAIN_URL}/runChecker.php`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            })
                .then((res) => {
                    if (res.data[0]) {
                        req.endpoint = res.data[0].filename
                        req.totalEmails = res.data[0].totalEmails
                        console.log('get filename and total emails')
                        console.log(req)
                        setReq({...req})
                        timeOut = setInterval(() => makeProgress(), 1000)
                    }
                })
                .catch(err => {
                    console.log(err)
                    setError('An error occurred, please try again later.')
                })
        }
    }


    const makeProgress = () => {
        const data = new FormData();
        data.append('filename', req.endpoint)
        axios.post(`${process.env.DOMAIN_URL}/progress.php`, data, options)
            .then((res) => {
                console.log('start progress')
                console.log(res.data)
                const progressPercentage = res.data === ' ' ? 0 : (parseInt(res.data) / parseInt(req.totalEmails)) * 100
                loader.isShow = true
                loader.progress = parseInt(progressPercentage)
                if (parseInt(res.data) === parseInt(req.totalEmails)) {
                    loader.showButtons = true
                    clearInterval(timeOut)
                }
                setLoader({...loader})
            })
            .catch(err => {
                setError('An error occurred, please try again later.')
                console.log(err)
            })
    }
    const getResults = () => {
        cnt++
        if (cnt === 2) {

            loader.isShow = false
            loader.showButtons = false
            Router.push('/', '/', {shallow: true});

        }
    }

    return (
        <>
            <form className={'d-flex flex-column clr-white'}>
                <div className={'d-flex align-items-center justify-content-between form-group'}>
                    <label htmlFor="file">{'Upload File'}</label>

                    <input type={"file"} name={'initial_file'} onChange={handleFile}/>

                </div>
                <div className={'d-flex align-items-center justify-content-between form-group'}>
                    <label htmlFor="maxConn">{'Max Connection Timeout (0-60)'}</label>
                    <div className={'pos-rel'}>
                        <p className={'pos-abs centerX'} style={{top: '-25px'}}>{maxValue}</p>
                        <input type={"range"} name={'max_conn_timeout'} step={'1'} id={'maxConn'} min={"0"}
                               max={"60"}
                               value={maxValue} onChange={handleRange}/>

                    </div>
                </div>
                <div className={'d-flex align-items-center justify-content-between form-group'}>
                    <label htmlFor="stream">{'Stream Timeout (0-60)'}</label>
                    <div className={'pos-rel'}>
                        <p className={'pos-abs centerX'} style={{top: '-25px'}}>{streamValue}</p>
                        <input type={"range"} name={'stream_timeout'} step={'1'} id={'stream'} min={"0"}
                               max={"60"}
                               value={streamValue} onChange={handleRange}/>

                    </div>
                </div>
                <div className={'d-flex align-items-center justify-content-between form-group'}>
                    <div className={'d-flex align-items-center flex-row-reverse'}>
                        <label htmlFor="dns">{'DNS Lookup'}</label>
                        <input type={"checkbox"} name={'dns_lookup_only'} id={'dns'} checked={dns}
                               onChange={handleCheckBox}/>
                    </div>
                    <div className={'d-flex align-items-center flex-row-reverse '}>
                        <label htmlFor="comma">{'Separate with comma '}</label>
                        <input type={"checkbox"} name={'comma_delimiter'} id={'comma'} checked={comma}
                               onChange={handleCheckBox}/>
                    </div>
                </div>
                <div className={'d-flex align-items-center justify-content-between'}>
                    <div className={'pr-button'} onClick={handleSubmit}>{'Submit'}</div>
                    <div className={'error'}><p>{error}</p></div>
                </div>

            </form>

            {loader.isShow &&

            <div className={'progress-overlay'}>
                <h4 className={'clr-white text-center'}>{`Progress:${loader.progress}%`}</h4>
                <div className="progress">
                    <div style={{width: `${loader.progress}%`}}/>
                </div>
                {loader.showButtons &&
                <div className={'pos-abs buttons-container'} onClick={getResults}>
                    <a className={'pr-button'} data-ref={'fail'}
                       href={`${process.env.DOMAIN_URL}/generated/failed/${req.endpoint}`}
                       target={'_blank'}>{'Failed List'}</a>
                    <a className={'pr-button'} data-ref={'success'}
                       href={`${process.env.DOMAIN_URL}/generated/succeeded/${req.endpoint}`}
                       download={`${req.endpoint}`} target={'_blank'}>{'Succeeded List'}</a>
                </div>
                }
            </div>
            }

        </>
    )
}
const getCookie = (name) => {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}

export default UploadForm