module.exports = {
  apps : [{
    name: 'checkMail',
    script: 'server.js',
    watch: true,
    env: {
      "PORT": process.env.PORT,
      "NODE_ENV": process.env.NODE_ENV,
    },
    env_production: {
      "PORT": process.env.PORT,
      "NODE_ENV": process.env.NODE_ENV,
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
