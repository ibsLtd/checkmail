import React from 'react'
import Layout from "../components/layout";
import UploadForm from "../components/uploadForm";
import Banner from "../components/banner";
import Rocket from '../components/rocket'
import Contact from  '../components/contact'

const _Upload = props => {

    return (
        <Layout>
            <section className={'pos-rel upload'}>
                <Banner class={'bottom'} height={'medium-height'}/>
                <div className={'pos-abs centerX overview'}>
                    <div className={'container'}>
                        <div className={'row justify-content-center'}>
                            <div className={'col-12 text-center clr-white'}>
                                <h1>{'Email Inspector'}</h1>
                                <h3>{'Real-time email verification'}</h3>
                            </div>
                            <div className={'col-md-6'}>
                                <UploadForm/>

                            </div>
                        </div>
                    </div>
                </div>
                <div className={'why-choose-us padding-btm how-to-use'}>
                    <div className={'container'}>
                        <div className={'row'}>
                            <div className={'col-12 text-center padding-btm'}>
                                <h1>{'Three easy steps'}</h1>
                                <p>{'It’s like a washing machine for emails.'}</p>
                            </div>
                            <div className={'col-md-4 text-center'}>
                                <h3>{'Hand in your dirty list.'}</h3>
                                <p>{'Upload your list with dirty emails into system'}</p>
                            </div>
                            <div className={'col-md-4 text-center'}>
                                <h3>{'Get a cup of coffee.'}</h3>
                                <p>{'Sit back and grab a cup of  while we work.'}</p>
                            </div>
                            <div className={'col-md-4 text-center'}>
                                <h3>{'Squeaky clean emails.'}</h3>
                                <p>{'Download your list, all clean and ready for your next email campaign.'}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className ={'pos-rel contact '}>
                    <Banner class={'top'} height={'large-height'}/>
                    <div className={'pos-abs center container'}>

                        <div className={'row'}>
                            <div className={'col-12 text-center clr-white padding-btm'}>
                                <h1>{'Contact Us'}</h1>
                            </div>

                            <div className={'col-md-5 clr-white text-center'}>
                                <h3>{'Contact Details'}</h3>
                                <p >{'For more information, please visit our website at : '} <a  rel="noopener" target="_blank" href="https://novatechsolutions.gr/">{'Novatech Solutions'}</a></p>
                            </div>
                            <div className={'col-md-7'}>
                                <Contact/>
                            </div>
                        </div>
                    </div>

                </div>
                <Rocket/>
            </section>


        </Layout>
    )
}

export default _Upload
