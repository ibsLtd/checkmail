import React from 'react'
import Layout from "../components/layout";
import Banner from "../components/banner";
import Overview from '../components/overview'
import Form from "../components/form"
import WhyChooseUs from "../components/whyChooseUs";
import Rocket from "../components/rocket";

const Home = props => {
    return (
        <Layout>
            <section className={'pos-rel homepage'}>
                <Banner class={'bottom'} height={'large-height'}/>
                <div className={'pos-abs centerX overview'}>
                    <div className={'container'}>
                        <div className={'row align-items-center'}>
                            <Overview/>
                            <div className={'col-md-6 d-flex justify-content-center'}>
                                <Form ip={props.ip}/>
                            </div>
                        </div>
                    </div>
                </div>
                <WhyChooseUs/>
                <div className={'pos-rel features'}>
                    <Banner class={'top'} height={'medium-height'}/>
                    <div className={'pos-abs centerX  container '}>
                        <div className={'row justify-content-between'}>

                            <div className={'col-md-4'}>
                                <div className={'bg-img  paperplane'}/>
                            </div>
                            <div className={'col-md-7 clr-white'}>
                                <h1>{'Improve deliverability of your next campaign'}</h1>
                                <p>{'Based on billions of emails we checked, about 23% of emails in your list may be invalid. Don’t let them lower the deliverability of your emails and your reputation score. Get rid of them and boost the open rate and CTR of your cool email marketing campaigns.'}</p>
                                <Form ip={props.ip}/>
                            </div>
                        </div>
                    </div>

                </div>
                <Rocket/>
            </section>
        </Layout>
    )
}

Home.getInitialProps = async ({query}) => {
    return {ip: query.ip}
}
export default Home
