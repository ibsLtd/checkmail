const express = require('express')
const next = require('next')
const bodyParser = require("body-parser");
const mysql = require('./modal/database')
const cookieParser = require('cookie-parser')
require('dotenv').config()

const port = parseInt(process.env.PORT, 10)
const dev = process.env.NODE_ENV
const app = next({dev})
const handle = app.getRequestHandler()

const insert = require('./modal/insert')


app.prepare().then(() => {

    const server = express()
    server.use(cookieParser())
    server.use(bodyParser.urlencoded({extended: true}));
    server.use(bodyParser.json());
    server.use("/public", express.static(__dirname + "/public", {
        maxAge: "365d"
    }))
    // Add a handler to inspect the req.secure flag (see
    // http://expressjs.com/api#req.secure). This allows us
    // to know whether the request was via http or https.
    // server.use(function (req, res, next) {
    //     if (req.secure) {
    //         // request was via https, so do no special handling
    //         next();
    //     } else {
    //         // request was via http, so redirect to https
    //         res.redirect('https://' + req.headers.host + req.url);
    //     }
    // });
    mysql.getConnection((err, connection) => {
        if (err) console.log(err)
        console.log("Connected!");
    })

    server.get('/', (req, res) => {
        const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
        return app.render(req, res, '/index', {ip: ip})
    })

    server.post('/', async (req, res) => {

        const date = new Date()
        res.cookie('user', req.body.email)

        try {
            const query = await insert(mysql, {ip: req.body.ip, date: date.toISOString(), email: req.body.email})
            return await res.send(query)

        } catch (err) {
            console.log(err)
            return await res.send(err)
        }

    })
    server.get('/upload', (req, res) => {
        if (req.cookies.user) return app.render(req, res, '/upload', req.query)
        else res.redirect('/')
    })

    server.all('*', (req, res) => {
        //res.redirect('https://' + req.headers.host + req.url);

        return handle(req, res)
    })

    server.listen(port, err => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
}).catch(ex => {

    console.error(ex.stack);
    process.exit(1);

});